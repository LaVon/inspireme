package com.kverchi.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.kverchi.dao.CountryDAO;
import com.kverchi.domain.Country;

@Service
public class CountryImpl implements CountryService {
	@Autowired 
	private CountryDAO countryDAO;
	
	@Override
	public Country findCountry(String code) {
		Country country = null;
		country = countryDAO.getCountry(code);
		return country;
	}

}
