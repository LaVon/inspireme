package com.kverchi.dao;

import com.kverchi.domain.Country;

public interface CountryDAO {
	public Country getCountry(String code);
}
