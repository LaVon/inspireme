<html>
	<head>
	<%@ include file="include.jspf" %>
	<title>Home</title>
	</head>
	<body>
		<div id="wrapper">
		  <%@ include file="head.jspf" %>
		  <div id="content">
			<div id="country">
				<img src="<%=request.getContextPath() %>/countryImg/${country.imgPath}"/>
			</div>
		  <p>${country.countryName}</p>
		  </div>
		  <%@ include file="footer.jspf" %>
		</div>
	</body>
</html>