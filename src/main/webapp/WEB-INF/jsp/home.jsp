<html>
	<head>
	<%@ include file="include.jspf" %>
	<title>Home</title>
	</head>
	<body>
		<div id="wrapper">
			<%@ include file="head.jspf" %>
			<div id="content">
				<p>It is a secret page. It's shown only for authorization users.</p>
			<div id="vmap" style="width: 600px; height: 400px;">
			<p><a href="<%=request.getContextPath() %>/main/country?code=CZ">Test me</a></p>
			</div>
			</div>
			<%@ include file="footer.jspf" %>
		</div>
	</body>
</html>